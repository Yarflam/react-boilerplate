import express from 'express';
import http from 'http';
import path from 'path';

import config from './config/public.cfg.json';

/* Define the server */
const app = express();
const server = http.Server(app);

app.use('/', express.static(path.resolve(__dirname, ...config.publish.expose)));

/* Listener */
server.listen(config.publish.port, '0.0.0.0', () => {
    console.log('The server is started on port ' + config.publish.port);
});

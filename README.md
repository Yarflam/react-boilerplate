# React Boilerplate

![react](https://img.shields.io/badge/react-16.6.3-blue.svg)
![babel](https://img.shields.io/badge/babel-7.2.0-orange.svg)
![license](https://img.shields.io/badge/license-CC_BY--NC--SA-green.svg)

## Install

It's really simple:

> $ npm i

You can run this command to begin:

> $ npm start

## Authors

- Yarflam - *initial work*

## License

The project is licensed under Creative Commons (BY-NC-SA) - see the [LICENSE.md](LICENSE.md) file for details.

import React, { Component } from 'react';
import styles from './app.scss';

class App extends Component {
    render() {
        return (
            <div className={styles.root}>
                Boilerplate is <span>ready</span> ! <i>by Yarflam</i>
            </div>
        );
    }
}

export default App;

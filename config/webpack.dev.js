const webpackSets = require('./webpack.sets');

module.exports = {
    mode: 'development',
    entry: './src/index.js',
    output: {
        path: webpackSets.public_path,
        filename: 'app.bundle.js'
    },
    module: {
        rules: [webpackSets.jsx, webpackSets.scss, ...webpackSets.fonts]
    },
    plugins: [],
    ...webpackSets.devmode
};

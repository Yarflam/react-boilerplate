const postcssPresetEnv = require('postcss-preset-env');

module.exports = {
    plugins: [
        require('autoprefixer'),
        require('postcss-flexbugs-fixes'),
        require('postcss-import'),
        require('postcss-for'),
        require('postcss-mixins'),
        require('postcss-simple-vars'),
        require('postcss-calc'),
        postcssPresetEnv({
            stage: 0,
            features: {
                'nesting-rules': true,
                'color-mod-function': true,
                'custom-media': true,
            },
        }),
    ],
};
